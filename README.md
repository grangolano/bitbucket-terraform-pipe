# Bitbucket Pipelines Pipe: terraform

Init and Apply Terraform

## YAML Definition

Add the following snippet to the script section of your `bitbucket-pipelines.yml` file:

```yaml
script:
  - pipe: docker://jangolano/terraform-pipe:latest
    variables:
      AWS_ACCESS_KEY: "<string>"
      AWS_SECRET_KEY: "<string>"
      TERRAFORM_DIRECTORY: "<string>"
      AWS_REGION: "<string>"
```
## Variables

| Variable              | Usage                                                       |
| --------------------- | ----------------------------------------------------------- |
| AWS_ACCESS_KEY (*)              | The AWS ACCESS KEY to be used |
| AWS_SECRET_KEY (*)              | The AWS SECRET KEY to be used |
| TERRAFORM_DIRECTORY (*)              | The directory where the terraform files are located |
| AWS_REGION (*)              | The aws region to deploy to us-east-1 |

_(*) = required variable._


## Examples

Example:

```yaml
script:
  - pipe: docker://jangolano/terraform-pipe:latest
    variables:
      AWS_ACCESS_KEY: "123abce"
      AWS_SECRET_KEY: "456sdfs13eresfsf"
      TERRAFORM_DIRECTORY: "terraform"
      AWS_REGION: "us-east-1"
```

## Support
If you are reporting an issue, please include:
- the version of the pipe
- relevant logs and error messages
- steps to reproduce