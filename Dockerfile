FROM hashicorp/terraform:0.12.8

RUN apk update && apk add bash

COPY pipe /
RUN chmod a+x /*.sh

ENTRYPOINT ["/pipe.sh"]
