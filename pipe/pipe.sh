#!/usr/bin/env bash
#
# Init and Apply Terraform
#

source "$(dirname "$0")/common.sh"

info "Executing the pipe..."

# Required parameters
AWS_ACCESS_KEY=${AWS_ACCESS_KEY:?'AWS_ACCESS_KEY is missing.'}
AWS_SECRET_KEY=${AWS_SECRET_KEY:?'AWS_SECRET_KEY is missing.'}
TERRAFORM_DIRECTORY=${TERRAFORM_DIRECTORY:?'TERRAFORM_DIRECTORY is missing.'}
AWS_REGION=${AWS_REGION:?'AWS_REGION is missing.'}


# Default parameters
DEBUG=${DEBUG:="false"}

#Script
terraform init -var "AWS_ACCESS_KEY=${AWS_ACCESS_KEY}" -var "AWS_SECRET_KEY=${AWS_SECRET_KEY}" -var "AWS_REGION=${AWS_REGION}" terraform
terraform apply -var "AWS_ACCESS_KEY=${AWS_ACCESS_KEY}" -var "AWS_SECRET_KEY=${AWS_SECRET_KEY}" -var "AWS_REGION=${AWS_REGION}" -auto-approve -input=false ${TERRAFORM_DIRECTORY}


if [[ "${status}" == "1" ]]; then
  fail "Error!"
else
  success "Success!"
fi
