# Changelog
Note: version releases in the 0.x.y range may introduce breaking changes.

## 0.1.3

- patch: Updated Version of Terraform

## 0.1.2

- patch: Updated Readme File

## 0.1.1

- patch: Fixed security vulnerablity with authentication.

## 0.1.0

- minor: Initial release

